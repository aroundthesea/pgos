# Perfectly Ground Work Orders
---
![Alt text](screenshots/list-view.png "list view")

![Alt text](screenshots/modal-view.png "modal view")

## Architecture
This is a Rails application with a classic MVC architecture.

There's an `order` model which houses all the orders and their data (I also seeded the database with some data for pagination testing purposes). The form should submit and add a new entry to the database (see **Trade-offs made** below). For all of this, I just used what came built-in with Rails and `Active Record`.

For the front-end, I'm using `Bootstrap 4` and `ERB` for styles and for pulling the data into the views.

## Trade-offs made
- I did not get to complete the form. Currently, you cannot submit the form to create a new entry in the database (I have seeded the database with data, so you can at least see and toggle the pagination). I would have liked to have hooked up the form, but due to work and time constraints, I was not able to.
- I did not get to write specs as I had hoped. I was hoping to write some feature specs with `capybara` to test the views: check if certain elements were on the page, click on create order, fill out the form, and submit. Within these specs, I would have tested the positive and negative cases (i.e. what happens if a field not filled out in the form). In addition, I would have liked to write specs for the model, to make sure that the correct columns are being created with the correct type of data.
- I did not follow the mocks exactly design-wise, also due to time constraints. There are a some details I chose to not implement, but would go back to do if I had more time (i.e. datepicker for the ship date field, sorting the ship date, pagination detail, form details, construct the actual Blue Bottle logo vs. using a screenshot image, etc).
- I did not make this mobile-friendly, but would do so if there were mocks provided for those views.
- I would have liked to clean up and fix the routing.
- I would also have liked to clean up the repo a bit, with adding more to the `.gitignore` and removing any unnecessary controllers and views (and general code cleanup as well).

## How to run it locally
Make sure you have at least `ruby 2.4.2`. If not, install it by running:

`rvm install ruby-2.4.2`

Clone the repo:

`git clone https://aroundthesea@bitbucket.org/aroundthesea/pgos.git`

Go into the root directory:

`cd <path-to>/pgos`

Then run:

`bundle install`

If you don't have bundler, run `gem install bundler`.

After that, set up the database:

`bundle exec rake db:migrate`

`bundle exec rake db:seed`

Then start the server:

`rails server` or `rails s`

Visit `localhost:3000` to view the page.

## Deployment and production readiness
I think that if I were to deploy this, I would likely use `heroku` for its easiness. Since I'm on Rails 5+, I would follow these instructions:

[https://devcenter.heroku.com/articles/getting-started-with-rails5](https://devcenter.heroku.com/articles/getting-started-with-rails5)

I think I would also need to use PostgreSQL, so I would probably want to set that up locally before pushing.

As for production readiness, it's not ready. There are no specs, the create order form does not work, and there is also no validation in place for the create order form. In addition, some form security should be in place as well (i.e. prevent spamming).