class MainController < ApplicationController
  def index
  	today = Date.today
  	@today_month = today.strftime("%b")
  	@today_day = today.strftime("%d")
  	@orders = Order.all.paginate(:page => params[:page], :per_page => 25)
  end
end
