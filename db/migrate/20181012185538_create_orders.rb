class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :coffee
      t.string :method
      t.string :number_of_cases
      t.string :packets_per_case
      t.date :ship_date
      t.string :order
      t.string :view

      t.timestamps null: false
    end
  end
end
